const express = require("express");
let exercise = express.Router();
let {
  updateExercise,
  getPlan
} = require("../controllers/exerciseController");
exercise.route("/updatePlan").post(updateExercise);
exercise.route("/getPlan").get(getPlan);
module.exports = exercise;
