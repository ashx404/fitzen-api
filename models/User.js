const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String
  },
  username: {
    type: String
  },
  email: {
    type: String
  },
  height: {
    type: Number
  },
  weight: {
    type: Number
  },
  caloriesArray: {
    type: Array
  },
  date: {
    type: Date,
    default: () => Date.now() + 5.5 * 60 * 60 * 1000
  },
  History: {
    type: Array,
    default: []
  },
  workoutPlan: {
    Monday: Array,
    Tuesday: Array,
    Wednesday: Array,
    Thursday: Array,
    Friday: Array,
    Saturday: Array,
    Sunday: Array
  },
  dietPlan: {
    Monday: Array,
    Tuesday: Array,
    Wednesday: Array,
    Thursday: Array,
    Friday: Array,
    Saturday: Array,
    Sunday: Array
  },
  exerciseCount: {
    Stretching: Number,
    Cardio: Number,
    Abs: Number,
    Hamstring: Number,
    Thigh: Number,
    Forearms: Number,
    Triceps: Number,
    Chest: Number,
    Posture: Number,
    Wrist: Number,
    Butt: Number,
    Neck: Number,
    Calves: Number
  },
  Avatar: {
    type: String,
    default:
      " https://enterfind.files.wordpress.com/2013/05/windows8_account_picture.png"
  },
  isSubscribed: { type: Boolean, default: false },
  role: { type: String, required: true },
  created: { type: String },
  google: {
    id: String,
    token: String,
    email: String,
    name: String
  }
});

mongoose.model("User", UserSchema);
module.exports = mongoose.model("User");
