const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Schema = mongoose.Schema;

const MentorSchema = new Schema({
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  email: {
    type: String
  },
  password: {
    type: String
  },
  certification: {
    type: Buffer
  },
  experience: {
    type: String
  },
  clients: {
    type: Array
  },
  date: {
    type: Date,
    default: () => Date.now() + 5.5 * 60 * 60 * 1000
  },
  about: {
    type: String
  },
  sessionCharges: {
    type: Number
  },
  verified: {
    type: Boolean,
    default: false
  },
  Avatar: {
    type: String,
    default:
      " https://enterfind.files.wordpress.com/2013/05/windows8_account_picture.png"
  },
  role: { type: String, required: true }
});
// MentorSchema.pre("save", async function() {
//   this.password = await bcrypt.hash(this.password, 12);
// });

mongoose.model("Mentor", MentorSchema);
module.exports = mongoose.model("Mentor");
