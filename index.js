const app = require("express")();
const cors = require("cors");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const User = require("./models/User");
const Mentor = require("./models/Mentor");
//const exercise = require("./routes/exercise");
const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
let port = process.env.PORT || 8888;
var authToken;
const mongoURI =
  "mongodb+srv://ashx404:qwe123@cluster0-9cinx.mongodb.net/test?retryWrites=true&w=majority";
//const mongoURI = "mongodb://localhost:27017/fitsen";
const fs = require("fs");
let data = fs.readFileSync("./exercise.json");
data = JSON.parse(data);

mongoose
  .connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("MongoDB Connected"))
  .catch(err => console.log(err));

app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
//app.use("/exercise", exercise);

passport.serializeUser((user, done) => {
  done(null, user._id);
});

process.env.SECRET_KEY = "sf3$$TDVd";

passport.use(
  new GoogleStrategy(
    {
      clientID:
        "1020794267311-foglek9s0lviqmqo3cd65qaa2lp8s5sj.apps.googleusercontent.com",
      clientSecret: "xWTRKCUlwgyX2nsA-U5ptoYX",
      callbackURL: "/auth/google/callback"
    },
    function(accessToken, refreshToken, profile, done) {
      console.log(profile);

      User.findOne({ "google.email": profile.emails[0].value }).then(
        currentUser => {
          if (currentUser) {
            console.log("User", currentUser);
            done(null, currentUser);
          } else {
            const today = new Date();
            const data = {
              google: {
                id: profile.id,
                email: profile.emails[0].value,
                name: profile.displayName
              },
              email: profile.emails[0].value,
              Avatar: profile.photos[0].value,
              created: today,
              role: "Customer"
            };
            User.create(data)
              .then(newUser => {
                console.log("New user created:" + newUser);

                done(null, newUser);
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      );
    }
  )
);

//customer apis
app.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email"
    ]
  })
);

app.get(
  "/auth/google/callback",
  passport.authenticate("google"),
  (req, res) => {
    const payload = {
      _id: req.user._id,
      name: req.user.google.name,
      email: req.user.google.email,
      avatar: req.user.Avatar
    };
    let token = jwt.sign(payload, process.env.SECRET_KEY, {
      expiresIn: "7d"
    });
    console.log(token);
    // res.send(token);\
    authToken = { token };
    res.setHeader("Authorization", "Bearer " + token);
    res.redirect("http://localhost:3000/auth");
  }
);

app.get("/getToken", (req, res) => {
  if (!authToken) {
    res.statusCode(400);
  } else {
    res.json(authToken);
  }
});

app.get("/getExercise", (req, res) => {
  let time = Number(req.query.time);
  let bodyPart = req.query.bodyPart;
  // console.log(req.query);

  // try {
  let data = fs.readFileSync("./exercise.json");
  data = JSON.parse(data);

  let myArray, myArrayFinal;
  // if (bodyPart == null) {
  if (time <= 900) {
    myArray = data[bodyPart];
    //call cardio stretching for given minutes
    let time1 = Math.ceil(time / 60);
    let nb_picks = Math.ceil(time1 / 5);
    // console.log(myArray);
    // // myArray = data[bodyPart];
    // console.log(myArray);
    myArray = fisherYates(myArray, nb_picks);
    // console.log(myArray,nb_picks);

    //   //cardio for 15 and bodypart for 15 minutes
  } else {
    //cardio for 20 and full exercise of any 1 body part
    let remTime = time - 900;
    remTime = Math.ceil(remTime / 60);
    let nb_picks = Math.ceil(remTime / 5);
    myArray = fisherYates(data["Stretching"], nb_picks);
    // console.log(data[bodyPart]);
    let data21 = data[bodyPart];
    for (let i in data21) {
      myArray.push(data21[i]);
    }

    // console.log(myArrayFinal);
  }

  // } else {
  //body part specified
  // let myArray = data[bodyPart];
  // if (time <= 900) {
  // let time1 = Math.ceil(time1 / 60);
  // nb_picks = Math.ceil(time1 / 5);
  // myArray = fisherYates(myArray, nb_picks);
  // }

  //show limited exercise acc to time
  // else {
  // myArray = data[bodyPart];
  //show full exercise of that body part
  // }
  // }
  // console.log(myArray);

  res.json({
    myArray
  });
  // } catch (err) {
  //   // console.log(err);
  //   res.status(401).json({
  //     status: "bad request"
  //   });
  // }
});

app.get("/getStats", async (req, res) => {
  let email = req.query.email;
  var result = await User.find({ email: email });
  // console.log(result[0]["History"]);
  // console.log(len);]
  let weekArray = [];
  let len = result[0]["History"].length;
  // let history = result[0]["History"];

  if (len == 0) {
    res.json({
      data: null
    });
  } else {
    let diffDays = 0;
    let b = moment(new Date(), "D/M/YYYY");
    let a = result[0]["History"][len - 1]["date"];
    var d = moment(moment.unix(a).format("D/M/YYYY"), "D/M/YYYY");
    diffDays = b.diff(d, "days");
    console.log(diffDays);

    for (var i = 0; i < 7; i++) {
      if (len >= 0 && weekArray.length <= 7 && diffDays <= 1) {
        weekArray.push(result[0]["History"][len - 1]["date"]);
        len--;
      } else {
        weekArray.push(0);
        diffDays--;
      }
      if (len >= 0 && diffDays <= 1) {
        let a = result[0]["History"][len - 1]["date"];
        diffDays = b.diff(a, "days");
      }
    }
  }
  console.log(weekArray);
});

let dataCode = fs.readFileSync("./bodyId.json");
dataCode = JSON.parse(dataCode);
let calorie = fs.readFileSync("./calorie.json");
calorie = JSON.parse(calorie);
const moment = require("moment");

app.get("/saveData", async (req, res) => {
  let emailId = req.query.email;
  // console.log(req.query.list)
  let obj = JSON.parse(req.query.list);
  let list = obj.ids;
  // console.log(list)
  let exerciseList = [];
  for (let i in list) {
    exerciseList.push(dataCode[list[i].charAt(0)]);
  }
  // console.log(exerciseList);
  try {
    //   let updatedObj={"workoutPlan":{
    //     "Monday":Array,
    //     "Tuesday":Array,
    //     "Wednesday":Array,
    //     "Thursday":Array,
    //     "Friday":Array,
    //     "Saturday":Array,
    //     "Sunday":Array
    //   },
    //   exerciseCount: {
    //   },
    //     caloriesArray:{
    //     type:Array
    //   },
    // }

    // var result = await UserModel.findAndUpdate({"email":emailId},updatedObj,{ new: true });

    let currData = {
      Stretching: 0,
      Cardio: 0,
      Abs: 0,
      Hamstring: 0,
      Thigh: 0,
      Forearms: 0,
      Triceps: 0,
      Chest: 0,
      Posture: 0,
      Wrist: 0,
      Butt: 0,
      Neck: 0,
      Calves: 0,
      Eyes: 0,
      Calories: 0,
      Time: 0,
      date: 0
    };

    var result = await User.find({ email: emailId });
    let user;
    if (result.length == 0) {
      let user = new User();
      user.role = "user";
      user.email = emailId;
      user.History = currData;
      user.date = new Date();
      result = await User.create(user, { new: true });

      // res.json({ data: "no email" });
    }

    // console.log(result+"cc");
    // result = Array(result);
    console.log(result[0]);

    let len = result[0].History.length;
    let cal;
    if (len == 0) {
      console.log(
        "*************************************************************************"
      );

      for (let i in exerciseList) {
        currData[exerciseList[i]] = currData[exerciseList[i]] + 1;
      }

      var zz = moment("26/01/2020", "DD/MM/YYYY");
      // console.log(zz);

      currData["date"] = zz;
      console.log(currData);
      console.log("##########################################");
      result[0]["History"] = currData;

      for (let i in exerciseList) {
        result[0]["History"][0]["Calories"] =
          result[0]["History"][0]["Calories"] + calorie[exerciseList[i]];
      }
      cal = result[0]["History"][0]["Calories"];
      result[0]["History"][0]["Time"] = exerciseList.length * 5;
    } else {
      let a = result[0]["History"][len - 1]["date"];
      let b = moment(27 / 1 / 2020, "D/M/YYYY");
      var diffDays = b.diff(a, "days");

      if (diffDays == 0) {
        console.log(
          "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        );

        console.log(result[0]["History"]);
        console.log(
          "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
        );
        console.log(exerciseList);
        for (let i in exerciseList) {
          console.log(result[0]["History"]);

          result[0]["History"][len - 1][exerciseList[i]] =
            result[0]["History"][len - 1][exerciseList[i]] + 1;
        }

        console.log(
          "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
        );
      } else {
        for (let i in exerciseList) {
          currData[exerciseList[i]] = currData[exerciseList[i]] + 1;
        }
        currData["date"] = moment(27 / 1 / 2020, "D/M/YYYY");
        result[0]["History"].push(currData);
      }

      for (let i in exerciseList) {
        result[0]["History"][len - 1]["Calories"] =
          result[0]["History"][len - 1]["Calories"] + calorie[exerciseList[i]];
      }
      result[0]["History"][len - 1]["Time"] = exerciseList.length * 5;
      cal = result[0]["History"][len - 1]["Calories"];
    }

    // let date=new Date();
    // date=dateFormat(date,"dd/mm/yyyy");
    // var a = moment(new Date(), "D/M/YYYY");
    // var b = moment("12/12/2019", "D/M/YYYY");
    // var diffDays = b.diff(a, "days");
    console.log(result[0]);
    // var newvalues = {
    //   $set: {
    //     History: result[0]["History"]
    //   }
    // };
    // var result = await User.find({ email: emailId });

    var resultFinal = await User.findOneAndUpdate(
      { email: emailId },
      result[0],
      function(res, err) {
        console.log("1 document updated");
      }
    );
    console.log(resultFinal);

    res.status(201).json({ status: "user Data Updated", calorie: cal });
  } catch (err) {
    console.log(err);
    res.send(err);
  }
});

app.get("/saveData", async (req, res) => {
  let emailId = req.query.email;
  // console.log(req.query.list)
  let obj = JSON.parse(req.query.list);
  let list = obj.ids;
  // console.log(list)
  let exerciseList = [];
  for (let i in list) {
    exerciseList.push(dataCode[list[i].charAt(0)]);
  }
  // console.log(exerciseList);
  try {
    //   let updatedObj={"workoutPlan":{
    //     "Monday":Array,
    //     "Tuesday":Array,
    //     "Wednesday":Array,
    //     "Thursday":Array,
    //     "Friday":Array,
    //     "Saturday":Array,
    //     "Sunday":Array
    //   },
    //   exerciseCount: {
    //   },
    //     caloriesArray:{
    //     type:Array
    //   },
    // }

    // var result = await UserModel.findAndUpdate({"email":emailId},updatedObj,{ new: true });
    var result = await User.find({ email: emailId });
    // console.log(result+"cc");

    console.log(result);

    let currData = {
      Stretching: 0,
      Cardio: 0,
      Abs: 0,
      Hamstring: 0,
      Thigh: 0,
      Forearms: 0,
      Triceps: 0,
      Chest: 0,
      Posture: 0,
      Wrist: 0,
      Butt: 0,
      Neck: 0,
      Calves: 0,
      Eyes: 0,
      Calories: 0,
      Time: 0,
      date: 0
    };
    let len = result[0]["History"].length;
    let cal;
    if (len == 0) {
      console.log(
        "*************************************************************************"
      );

      for (let i in exerciseList) {
        currData[exerciseList[i]] = currData[exerciseList[i]] + 1;
      }

      var zz = moment("26/01/2020", "DD/MM/YYYY");
      // console.log(zz);

      currData["date"] = zz;
      console.log(currData);
      console.log("##########################################");
      result[0]["History"] = currData;

      for (let i in exerciseList) {
        result[0]["History"][0]["Calories"] =
          result[0]["History"][0]["Calories"] + calorie[exerciseList[i]];
      }
      cal = result[0]["History"][0]["Calories"];
      result[0]["History"][0]["Time"] = exerciseList.length * 5;
    } else {
      let a = result[0]["History"][len - 1]["date"];
      let b = moment(27 / 1 / 2020, "D/M/YYYY");
      var diffDays = b.diff(a, "days");

      if (diffDays == 0) {
        console.log(
          "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        );

        console.log(result[0]["History"]);
        console.log(
          "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
        );
        console.log(exerciseList);
        for (let i in exerciseList) {
          console.log(result[0]["History"]);

          result[0]["History"][len - 1][exerciseList[i]] =
            result[0]["History"][len - 1][exerciseList[i]] + 1;
        }

        console.log(
          "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
        );
      } else {
        for (let i in exerciseList) {
          currData[exerciseList[i]] = currData[exerciseList[i]] + 1;
        }
        currData["date"] = moment(27 / 1 / 2020, "D/M/YYYY");
        result[0]["History"].push(currData);
      }

      for (let i in exerciseList) {
        result[0]["History"][len - 1]["Calories"] =
          result[0]["History"][len - 1]["Calories"] + calorie[exerciseList[i]];
      }
      result[0]["History"][len - 1]["Time"] = exerciseList.length * 5;
      cal = result[0]["History"][len - 1]["Calories"];
    }

    // let date=new Date();
    // date=dateFormat(date,"dd/mm/yyyy");
    // var a = moment(new Date(), "D/M/YYYY");
    // var b = moment("12/12/2019", "D/M/YYYY");
    // var diffDays = b.diff(a, "days");
    console.log(result[0]);
    // var newvalues = {
    //   $set: {
    //     History: result[0]["History"]
    //   }
    // };
    // var result = await User.find({ email: emailId });

    var resultFinal = await User.findOneAndUpdate(
      { email: emailId },
      result[0],
      function(res, err) {
        console.log("1 document updated");
      }
    );
    console.log(resultFinal);

    res.status(201).json({ status: "user Data Updated", calorie: cal });
  } catch (err) {
    console.log(err);
    res.send(err);
  }
});

function fisherYates(myArray, nb_picks) {
  for (i = myArray.length - 1; i > 1; i--) {
    var r = Math.floor(Math.random() * i);
    var t = myArray[i];
    myArray[i] = myArray[r];
    myArray[r] = t;
  }
  //  console.log(myArray.slice(0, nb_picks));
  return myArray.slice(0, nb_picks);
}

app.post("/newMentor", (req, res) => {
  console.log(req.params);
  console.log(req.body);

  let userdata = {
    firstname: req.body.firstName,
    lastname: req.body.lastName,
    email: req.body.email,
    password: req.body.password,
    role: req.body.role,
    experience: req.body.experience,
    about: req.body.about,
    sessionCharges: req.body.sessionCharges
  };

  Mentor.create(userdata)
    .then(user => {
      res.json({
        user: user
      });
    })
    .catch(err => {
      res.send("error: " + err);
    });
});

//app.post("/login",(req,res))

app.post("/login", async (req, res) => {
  try {
    console.log(req.body);

    let data = req.body;
    let { email, password } = data;
    let userData = await Mentor.findOne({ email });

    if (!userData) {
      res.json({ Failed: "User doesn't exists" });
      res.end();
    } else {
      console.log(userData.firstname + " " + userData.lastname);
      const JWTtoken = jwt.sign(
        {
          id: userData._id,
          email: userData.email,
          role: userData.role,
          fname: userData.firstname,
          lname: userData.lastname
        },
        process.env.SECRET_KEY,
        { expiresIn: "7d" }
      );
      res.send({ JWTtoken });
    }
  } catch (err) {
    console.log(err);
    res.statusCode(500);
  }
});


app.get("/getTrainers", async (req, res) => {
  let obj = await Mentor.find({ role: "Trainer" });
  console.log();

  res.json({ obj });
});

app.get("/getNutris", async (req, res) => {
  let obj = await Mentor.find({ role: "Nutritionist" });
  console.log(obj);
  res.json({obj});
});

app.get("/getTheris", async (req, res) => {
  let obj = await Mentor.find({ role: "Therapist" });
  console.log(obj);
  res.json({obj});
  
});

app.get("/test", (req, res) => {
  res.send("sdsdsds");
});
app.listen(port, (req, res) => {
  console.log("Listening to port");
});
